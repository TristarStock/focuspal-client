﻿using FocusPal.Models;
using FocusPal.Models.Enums;
using System.Collections.Generic;
using System.Linq;

namespace FocusPal
{
    public static class Browsers
    {
        public static List<Browser> List = new List<Browser>()
        {
            new Browser() { BrowserType = BrowserType.Chrome,  AddressPropertyName = "Address and search bar",  ProcessName = "chrome"  },
            new Browser() { BrowserType = BrowserType.Firefox, AddressPropertyName = "Search or enter address", ProcessName = "firefox" },
            new Browser() { BrowserType = BrowserType.IE,      AddressPropertyName = "Address and search using Bing", ProcessName = "iexplore" },
            new Browser() { BrowserType = BrowserType.Edge,    AddressPropertyName = "Address and search bar", ProcessName = "msedge" }
        };

        public static Browser GetBrowserTypeByProcessName(string processName)
        {
            return List.SingleOrDefault(b => b.ProcessName == processName.Replace(".exe", ""));
        }
    }
}