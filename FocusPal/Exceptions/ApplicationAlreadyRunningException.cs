﻿using System;

namespace FocusPal
{
    public class ApplicationAlreadyRunningException : Exception
    {
        public ApplicationAlreadyRunningException()
        {
        }

        public ApplicationAlreadyRunningException(string message) 
            : base(message)
        {
        }

        public ApplicationAlreadyRunningException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}