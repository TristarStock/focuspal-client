﻿using FocusPal.Abstractions;
using FocusPal.Models;
using FocusPal.Models.Enums;
using System.Diagnostics;
using System.Windows;

namespace FocusPal
{
    public partial class MainWindow : Window
    {
        private readonly IContextMenuViewModel contextMenuViewModel;
        private readonly IVelocityService velocityService;

        public MainWindow(IContextMenuViewModel contextMenuViewModel, IVelocityService velocityService)
        {
            InitializeComponent();
            this.DataContext = contextMenuViewModel;
            this.contextMenuViewModel = contextMenuViewModel;
            this.velocityService = velocityService;
            contextMenuViewModel.TooltipNeeded += ContextMenuViewModel_PopupNeeded;
        }
        private void ContextMenuViewModel_PopupNeeded(string title)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (tb.CustomBalloon == null)
                {
                    var tooltip = new PalToolTip(title);
                    tooltip.PalToolTipClosing += (object? sender, System.EventArgs e) => tb.CloseBalloon();
                    tb.ShowCustomBalloon(tooltip, System.Windows.Controls.Primitives.PopupAnimation.Slide, 6000);
                }
            });
        }
        private void Focus_Click(object sender, RoutedEventArgs e) => contextMenuViewModel.Mode = contextMenuViewModel.Mode == AppMode.Focus ? AppMode.Active : AppMode.Focus; 
        private void Pause_Click(object sender, RoutedEventArgs e) => contextMenuViewModel.Mode = AppMode.Paused;
        private void Quit_Click(object sender, RoutedEventArgs e) => Application.Current.Shutdown(762);
        private void DisplayHourly_Click(object sender, RoutedEventArgs e) => contextMenuViewModel.SetDisplayProductivityHourly(true);
        private void DisplayTotal_Click(object sender, RoutedEventArgs e) => contextMenuViewModel.SetDisplayProductivityHourly(false);
        private void Reset_Click(object sender, RoutedEventArgs e) => velocityService.ResetMetrics();
        private void Documentation_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo
            {
                FileName = "https://gitlab.com/TristarStock/focuspal-client",
                UseShellExecute = true
            });
        }
    }
}