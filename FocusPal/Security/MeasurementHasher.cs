﻿using FocusPal.Models;
using System;
using System.Diagnostics;
using System.Security.Cryptography;
using ActivityEventModel = FocusPal.Models.ActivityEventModel;

namespace FocusPal
{
    public interface IMeasurementHasher
    {
        string Hash(ActivityEventModel e);
        string Hash(Measurement m);
        string Hash(string applicationName, string applicationTitle, string applicationTitleHash, string timeSegment);
    }
    public class MeasurementHasher : IMeasurementHasher
    {
        private static string? _pId = null;
        private readonly IUserOptions userOptions;

        public MeasurementHasher(IUserOptions userOptions)
        {
            this.userOptions = userOptions;
        }

        private string PId { 
            get { return _pId ??= GetProcessId(); }
        }

        public string Hash(ActivityEventModel e)
        {
            return Hash(e.ApplicationName, e.ApplicationTitle, e.ApplicationTitleHash, null);
        }

        /// <summary>
        /// This will hash using ApplicationName, ApplicationTitle and ApplicationShortTitle, instead of using TitleHash
        /// This function should be used to regroup same subdomain measurements before export.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public string Hash(Measurement m)
        {
            return Hash(m.ApplicationName, m.ApplicationTitle, m.ApplicationShortTitle, m.TimeSegment);
        }

        //applicationShortTitle is necessary here so that 'active domain' are tracked every 3 seconds instead of every 2 mins
        //with applicationShortTitle in hash (even if we received it as the md5 of its value),
        //two domains will become two distinct measurement records, per time chunk
        public string Hash(string applicationName, string applicationTitle, string applicationShortTitle, string timeSegment)
        {
            if(timeSegment == null)
                timeSegment = DateTime.Now.RoundDown(TimeSpan.FromMinutes(userOptions.ProfileConfiguration.FrequencyInMinutes)).ToString("yyyy-MM-dd HH:mm");
            
            string input = timeSegment + applicationName + applicationShortTitle;

            if (userOptions.ProfileConfiguration.IncludeTitles)
                input += applicationTitle;

            return Hasher.Hash(input);
        }

        private string GetProcessId()
        {
            var processes = Process.GetProcessesByName("explorer");
            if (processes.Length == 0)
            {
                return "";
            }

            return processes[0].Id.ToString();
        }
    }

    public static class Hasher
    {
        public static readonly MD5 md5 = MD5.Create();
        public static string Hash(string s)
        {
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(s);
            byte[] hashBytes  = md5.ComputeHash(inputBytes);

            return Convert.ToHexString(hashBytes);
        }
    }
}