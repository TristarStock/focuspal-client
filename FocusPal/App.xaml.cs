﻿using FocusPal.Abstractions;
using FocusPal.Models;
using FocusPal.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.IO;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Windows;

namespace FocusPal
{
    public partial class App : Application
    {
        private readonly IHost host;
        private IConfiguration configuration;
        private readonly ILogger<App> logger;
        private ISecurityService securityService;
        private ConfigurationBuilder configurationBuilder;

        public App()
        {
            configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            configuration = configurationBuilder.Build();

            var seriLog = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            host = Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    services.AddLogging(logging =>
                    {
                        logging.AddSerilog(seriLog);
                    });

                    ConfigureServices(services);
                }).Build();

            logger = host.Services.GetService<ILoggerFactory>().CreateLogger<App>();            
        }
        private void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IUserOptions,          UserOptions>();
            services.AddSingleton<IVelocityService,      VelocityService>();
            services.AddSingleton<IUserActivityService,  UserActivityService>();
            services.AddSingleton<ISecurityService,      SecurityService>();
            services.AddSingleton<IDataExportService,    DataExportService>();
            services.AddSingleton<IMeasurementsService,  MeasurementsService>();
            services.AddSingleton<IMeasurementHasher,    MeasurementHasher>();
            services.AddSingleton<IContextMenuViewModel, ContextMenuViewModel>();
            services.AddSingleton<IProcessManagementService, ProcessManagementService>();
            services.AddSingleton(typeof(IExceptionsService<>), typeof(ExceptionsService<>));

            services.AddHttpClient("", client =>
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", configuration["access_token"]);
                client.BaseAddress = new Uri(configuration["Endpoints:baseUrl"]);
            });

            services.Configure<AppOptions>(configuration.GetSection("AppOptions"));
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            logger.LogInformation("Initializing.");

            securityService = host.Services.GetService<ISecurityService>();
            securityService.CheckIfInstanceAlreadyRunning();
            logger.LogInformation("Checked Instances.");

            string profileOptionsJson = securityService.Authenticate();
            logger.LogInformation("Authenticated.");

            UserOptions userOptionsService = (UserOptions)host.Services.GetService<IUserOptions>();
            var userOptions = JsonSerializer.Deserialize<UserOptions>(profileOptionsJson, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
            userOptionsService.FocusMode = userOptions.FocusMode;
            userOptionsService.ProfileConfiguration = userOptions.ProfileConfiguration;

            var userActivityService = host.Services.GetService<IUserActivityService>();
            userActivityService.RegisterEventHandlers();
            userActivityService.RegisterHooks();
            logger.LogInformation("Registered hooks.");

            var dataExportService = host.Services.GetService<IDataExportService>();
            dataExportService.StartDomainResolverThread();
            dataExportService.StartDataExportThread();
            logger.LogInformation("Started threads.");

            base.OnStartup(e);

            new MainWindow(host.Services.GetService<IContextMenuViewModel>(), host.Services.GetService<IVelocityService>());
        }

        protected override async void OnExit(ExitEventArgs e)
        {
            logger.LogInformation($"Exiting - {e.ApplicationExitCode}");

            using (host)
            {
                await host.StopAsync();
            }

            base.OnExit(e);
        }
    }
}