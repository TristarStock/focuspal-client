﻿using System;
using System.Windows.Controls;

namespace FocusPal
{
    public partial class PalToolTip : UserControl
    {
        private string title;
        public PalToolTip(string title)
        {
            InitializeComponent();

            DataContext = this;

            this.title = title;
        }

        public string IconSource => "/Images/Statuses/focus.ico";
        public string Title { get { return title; } }

        public event EventHandler PalToolTipClosing;

        private void TextBlock_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
            => PalToolTipClosing.Invoke(sender, e);
    }
}