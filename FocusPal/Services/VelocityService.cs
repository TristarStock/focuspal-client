﻿using FocusPal.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FocusPal.Services
{
    public class VelocityService : IVelocityService
    {
        public VelocityService()
        {
        }
        public long TotalInches { get; private set; }
        private List<string> TimeSegments { get; set; } = new List<string>();
        public double Minutes { get => TimeSegments.Count * 2; }
        public int TotalClicksCount { get; private set; }
        public int TotalKeysCount { get; private set; }
        
        public event EventHandler MetricsChanged;

        public void IncreaseMetrics(IMeasurement m)
        {
            if(TimeSegments.SingleOrDefault(ts => ts == m.TimeSegment) == null)
                TimeSegments.Add(m.TimeSegment);

            TotalInches      += m.DistanceInches;
            TotalKeysCount   += m.KeyboardCounter;
            TotalClicksCount += m.MouseCounter;
        }

        //Calling this (user activates focus mode) close to the time at which the next export loop was going to run
        //would skew the metrics a bit for that 2 min span
        public void ResetMetrics()
        {
            TotalInches = 0;
            TotalKeysCount = 0;
            TimeSegments = new List<string>();
            TotalClicksCount = 0;

            MetricsChanged.Invoke(this, null);
        }

    }
}