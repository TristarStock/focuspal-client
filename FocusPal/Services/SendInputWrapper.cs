﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace FocusPal.Services
{
    internal class User32SendInput
    {
        //References:
        //https://stackoverflow.com/questions/71587520/how-to-use-sendinput-to-simulate-the-up-arrow-key-press-or-other-extended-keys
        //https://docs.microsoft.com/en-us/windows/desktop/api/winuser/nf-winuser-sendinput
        //https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-input
        //https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-keybdinput

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern uint SendInput(uint nInputs, [In, MarshalAs(UnmanagedType.LPArray)] INPUT[] pInputs, int cbSize);

        public static bool SendKeyboardInput(Keys key, Keys modifier)
        {
            var inputs = new List<INPUT>();
            var input  = new INPUT(SendInputType.InputKeyboard);

            input.Union.Keyboard.Flags = KeyboardInputFlags.KeyDown;
            input.Union.Keyboard.VirtKeys = (ushort)modifier;
            inputs.Add(input);

            input.Union.Keyboard.Flags = KeyboardInputFlags.KeyDown;
            input.Union.Keyboard.VirtKeys = (ushort)key;
            inputs.Add(input);

            input.Union.Keyboard.Flags = KeyboardInputFlags.KeyUp;
            input.Union.Keyboard.VirtKeys = (ushort)key;
            inputs.Add(input);

            input.Union.Keyboard.Flags = KeyboardInputFlags.KeyUp;
            input.Union.Keyboard.VirtKeys = (ushort)modifier;
            inputs.Add(input);

            uint sent = SendInput((uint)inputs.Count(), inputs.ToArray(), Marshal.SizeOf<INPUT>());
            return sent > 0;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct INPUT
        {
            public SendInputType InputType;
            public InputUnion Union;
            public INPUT(SendInputType type)
            {
                InputType = type;
                Union = new InputUnion();
            }
        }
        public enum SendInputType : uint
        {
            InputMouse = 0,
            InputKeyboard = 1,
            InputHardware = 2
        }
        [StructLayout(LayoutKind.Explicit)]
        public struct InputUnion
        {
            [FieldOffset(0)]
            public MOUSEINPUT Mouse;

            [FieldOffset(0)]
            public KEYBDINPUT Keyboard;

            [FieldOffset(0)]
            public HARDWAREINPUT Hardware;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public uint mouseData;
            public MouseEventdwFlags dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct KEYBDINPUT
        {
            public ushort VirtKeys;
            public ushort wScan;
            public KeyboardInputFlags Flags;
            public uint time;
            public IntPtr dwExtraInfo;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct HARDWAREINPUT
        {
            public int uMsg;
            public short wParamL;
            public short wParamH;
        }
        [Flags]
        public enum MouseEventdwFlags : uint
        {
            MOUSEEVENTF_MOVE = 0x0001,
            MOUSEEVENTF_LEFTDOWN = 0x0002,
            MOUSEEVENTF_LEFTUP = 0x0004,
            MOUSEEVENTF_RIGHTDOWN = 0x0008,
            MOUSEEVENTF_RIGHTUP = 0x0010,
            MOUSEEVENTF_MIDDLEDOWN = 0x0020,
            MOUSEEVENTF_MIDDLEUP = 0x0040,
            MOUSEEVENTF_XDOWN = 0x0080,
            MOUSEEVENTF_XUP = 0x0100,
            MOUSEEVENTF_WHEEL = 0x0800,
            MOUSEEVENTF_VIRTUALDESK = 0x4000,
            MOUSEEVENTF_ABSOLUTE = 0x8000
        }
        [Flags]
        public enum KeyboardInputFlags : uint
        {
            KeyDown = 0x0,
            ExtendedKey = 0x0001,
            KeyUp = 0x0002,
            Scancode = 0x0008,
            Unicode = 0x0004
        }
    }
}