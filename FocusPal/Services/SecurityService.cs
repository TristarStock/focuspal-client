﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Net.Http;
using System.Reflection;
using System.Windows;

namespace FocusPal
{
    public interface ISecurityService
    {
        string Authenticate();
        void CheckIfInstanceAlreadyRunning();
    }
    public class SecurityService : ISecurityService
    {
        private readonly IExceptionsService<SecurityService> exceptionsService;
        private readonly IConfiguration configuration;
        private readonly IHttpClientFactory httpClientFactory;
        private readonly ILogger<SecurityService> logger;

        public SecurityService( IExceptionsService<SecurityService> exceptionsService, 
                                IConfiguration configuration, 
                                IHttpClientFactory httpClientFactory,
                                ILogger<SecurityService> logger)
        {
            this.exceptionsService = exceptionsService;
            this.configuration = configuration;
            this.httpClientFactory = httpClientFactory;
            this.logger = logger;
        }

        public void CheckIfInstanceAlreadyRunning()
        {
            if (Process.GetProcessesByName("focuspal").Length > 1 && !configuration["Endpoints:baseUrl"].Contains("localhost:"))
            {
                exceptionsService.ReportFatalException(new ApplicationAlreadyRunningException(), 333);
            }
        }

        public string Authenticate()
        {
            var payload = new
            {
                Token = configuration["access_token"],
                Version = Assembly.GetExecutingAssembly().GetName().Version
            };

            try
            {
                var httpClient = httpClientFactory.CreateClient();

                var response = httpClient.PostAsJsonAsync(configuration["Endpoints:Authenticate"], payload).Result;

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    logger.LogCritical("Auth failed.");
                    Application.Current.Shutdown(111);
                    return string.Empty;
                }

                var content = response.Content.ReadAsStringAsync().Result;

                return content;
            }
            catch (System.Exception ex)
            {
                exceptionsService.ReportFatalException(ex, 332);
#if DEBUG
                throw;
#endif
            }

            return null; // required by dotnet publish command line
        }
    }
}