﻿using FocusPal.Models;
using System.Collections.Concurrent;

namespace FocusPal.Services
{
    public interface IMeasurementsService
    {
        ConcurrentDictionary<string, Measurement> Measurements { get; set; }
    }
    public class MeasurementsService : IMeasurementsService
    {
        public ConcurrentDictionary<string, Measurement> Measurements { get; set; } = new ConcurrentDictionary<string, Measurement>();
    }
}