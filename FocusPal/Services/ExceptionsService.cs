﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Windows;

namespace FocusPal
{
    public interface IExceptionsService<T> where T : class
    {
        void ReportException     (Exception ex, [CallerMemberName] string method = "");
        void ReportFatalException(Exception ex, int exitCode, [CallerMemberName] string method = "");
    }
    public class ExceptionsServiceBase { }
    public class ExceptionsService<T> : ExceptionsServiceBase, IExceptionsService<T> where T : class
    {
        public ExceptionsService(ILogger<ExceptionsService<T>> logger, IConfiguration configuration, IHttpClientFactory httpClientFactory)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.httpClientFactory = httpClientFactory;
        }

        private readonly ILogger logger;
        private readonly IConfiguration configuration;
        private readonly IHttpClientFactory httpClientFactory;

        public void ReportException(Exception ex, [CallerMemberName] string method = "")
        {
            logger.LogError(ex, null);

            var httpClient = httpClientFactory.CreateClient();

            var payload = new ExceptionReport()
            {
                Exception = ex,
                Namespace = typeof(T).Namespace ?? "",
                ClassName = typeof(T).Name,
                Method    = method
            };

            try
            {
                var result = httpClient.PostAsJsonAsync(configuration["Endpoints:Exceptions"], payload).Result;
            }
            catch (Exception exception)
            {
                // no internet?
                logger.LogCritical(exception, null);
                Application.Current.Shutdown(222);
            }
        }

        public void ReportFatalException(Exception ex, int exitCode, [CallerMemberName] string method = "")
        {
            this.ReportException(ex, method);
            Application.Current.Shutdown(exitCode);
        }

        private class ExceptionReport
        {
            public DateTime DayDate { get; } = DateTime.Now;
            public Exception Exception { get; set; }
            public string Namespace { get; set; } = string.Empty;
            public string ClassName { get; set; } = string.Empty;
            public string Method { get; set; } = string.Empty;
        }
    }
}