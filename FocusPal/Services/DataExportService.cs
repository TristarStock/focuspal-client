﻿using FocusPal.Abstractions;
using FocusPal.Models;
using FocusPal.Models.Enums;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nager.PublicSuffix;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FocusPal.Services
{
    public interface IDataExportService
    {
        void Export(bool forceExport = false);
        void StartDataExportThread();
        void StartDomainResolverThread();
    }
    public class DataExportService : IDataExportService
    {
        private readonly IMeasurementsService measurementsService;
        private readonly ILogger<DataExportService> logger;
        private readonly IExceptionsService<DataExportService> exceptionsService;
        private readonly IUserOptions userOptions;
        private readonly IMeasurementHasher measurementHasher;
        private readonly IConfiguration configuration;
        private readonly IHttpClientFactory httpClientFactory;
        private readonly IVelocityService velocityService;
        private readonly IContextMenuViewModel contextMenuViewModel;

        public DataExportService(IMeasurementsService measurementsService, 
                                 ILogger<DataExportService> logger, 
                                 IExceptionsService<DataExportService> exceptionsService,
                                 IUserOptions userOptions,
                                 IMeasurementHasher measurementHasher,
                                 IConfiguration configuration,
                                 IHttpClientFactory httpClientFactory,
                                 IVelocityService velocityService,
                                 IContextMenuViewModel contextMenuViewModel)
        {
            this.logger = logger;
            this.exceptionsService = exceptionsService;
            this.userOptions = userOptions;
            this.measurementHasher = measurementHasher;
            this.configuration     = configuration;
            this.httpClientFactory = httpClientFactory;
            this.velocityService   = velocityService;
            this.contextMenuViewModel = contextMenuViewModel;
            this.measurementsService = measurementsService;
        }

        public void StartDataExportThread()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        int seconds = 10; //userOptions.ProfileConfiguration.FrequencyInMinutes * 60;
                        Thread.Sleep(TimeSpan.FromSeconds(seconds));

                        Export();
                    }
                    catch (Exception ex)
                    {
                        //System.ArgumentNullException: 'SafeHandle cannot be null. Arg_ParamName_Name'
                        //Value cannot be null. (Parameter 's')
                        //FocusPal.Hasher.Hash(string) in Hasher.

                        exceptionsService.ReportException(ex);
#if DEBUG
                        throw;
#endif
                    }
                }
            });
        }

        public void StartDomainResolverThread()
        {
            const int MaxRetries = 4, SleepInSeconds = 3;

            Stopwatch sw = new Stopwatch();

            Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        var missingDomains = measurementsService.Measurements.Where(m => m.Value.Browser != null && m.Value.Domain == string.Empty && m.Value.DomainRetries < MaxRetries);

                        if (missingDomains.Any())
                        {
                            for (int i = 0; i < missingDomains.Count(); i++)
                            {
                                var key = missingDomains.ElementAt(i).Key;
                                var e   = missingDomains.ElementAt(i).Value;

                                var missingDomain = missingDomains.ElementAt(i).Value;

                                string url = string.Empty;

                                IntPtr hWnd = missingDomain.HWnd;
                                var processId = 0;
                                Process processToClose = null;

                                foreach (Process process in Process.GetProcessesByName(missingDomain.Browser.ProcessName))
                                {
                                    if (process.MainWindowHandle == hWnd)
                                    {
                                        sw.Start();
                                        url = ProcessManagementService.GetURLFromProcess(process, missingDomain.Browser);
                                        Debug.WriteLine($"GetURLFromProcess {sw.ElapsedMilliseconds}ms");
                                        sw.Reset();

                                        if (String.IsNullOrEmpty(url))
                                            continue;
                                        else
                                        {
                                            processId = process.Id;
                                            processToClose = process;
                                            break;
                                        }
                                    }
                                }

                                var newValue = (Measurement)e.Clone();

                                try
                                {
                                    DomainInfo domainInfo = new DomainParser(new WebTldRuleProvider()).Parse(url);
                                    newValue.Domain = domainInfo.Hostname; // was a valid url with a valid domain

                                    if (contextMenuViewModel.Mode == AppMode.Focus)
                                    {
                                        if (userOptions.FocusMode.UnproductiveDomains.Contains(domainInfo.Hostname) ||
                                            userOptions.FocusMode.UnproductiveDomains.Contains(domainInfo.RegistrableDomain))
                                        {
                                            string unproductive = userOptions.FocusMode.UnproductiveDomains.Contains(domainInfo.Hostname) ? domainInfo.Hostname : domainInfo.RegistrableDomain;
                                            Debug.WriteLine($"unproductive: {unproductive}");

                                            bool result = User32SendInput.SendKeyboardInput(Keys.W, Keys.ControlKey);

                                            contextMenuViewModel.ShowTooltip($"Closed unproductive website\n{unproductive}");
                                        }
                                        else
                                            Debug.WriteLine($"productive: {domainInfo.Hostname}");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    if (++newValue.DomainRetries == MaxRetries)
                                    {
                                        //setting it as no domain, it was possibly a google search or just a non valid domain
                                        //url is null if watching a video (youtube at least) full screen
                                        newValue.Domain = "-";
                                        Debug.WriteLine($"set default domain for improper uri '{url}'");
                                    }
                                    else
                                        Debug.WriteLine($"will retry url '{url}' {(MaxRetries - newValue.DomainRetries)} more times");
                                }

                                var b = measurementsService.Measurements.TryUpdate(key, newValue, e);
                                if (!b)
                                {
                                    Debug.WriteLine($"Failed to update measurement - ApplicationName: {e.ApplicationName}");
                                }
                            }
                        }

                        Thread.Sleep(TimeSpan.FromSeconds(SleepInSeconds));
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        exceptionsService.ReportException(ex);
#if DEBUG
                        throw;
#endif
                        /*System.ArgumentOutOfRangeException: 'Specified argument was out of the range of valid values. Arg_ParamName_Name'*/
                    }
                    catch (Exception ex)
                    {
                        exceptionsService.ReportException(ex);
#if DEBUG
                        throw;
#endif
                    }
                }
            });
        }

        public void Export(bool forceExport = false)
        {
            DateTime currentSegment = DateTime.Now.RoundDown(TimeSpan.FromMinutes(userOptions.ProfileConfiguration.FrequencyInMinutes));

            DataExport payload = new DataExport();

            var measurementsOfPastTimeSegments = measurementsService.Measurements
                                                 .Where(x => Convert.ToDateTime(x.Value.TimeSegment) < currentSegment && 
                                                        x.Value.Exported == false).ToList();

            var httpClient = httpClientFactory.CreateClient();

            if (measurementsOfPastTimeSegments.Count() > 0)
            {
                for (int i = 0; i < measurementsOfPastTimeSegments.Count(); i++)
                {
                    var e = measurementsOfPastTimeSegments.ElementAt(i).Value;

                    int? windowId = payload.Windows.SingleOrDefault(w => measurementHasher.Hash(e) == w.Hash)?.Id;

                    if (windowId == null)
                    {
                        windowId = payload.Windows.Count > 0 ? payload.Windows.OrderByDescending(w => w.Id).First().Id + 1 : 1;

                        payload.Windows.Add(new ApplicationRecord()
                        {
                            Id = windowId.Value,
                            ApplicationName = e.ApplicationName,
                            ApplicationTitle = e.ApplicationTitle,
                            ApplicationShortTitle = e.ApplicationShortTitle,
                            Hash = measurementHasher.Hash(e)
                        });
                    }

                    e.DistanceInches = (long)(e.DistancePixels / 96);
                    e.WindowId = windowId.Value;
                    e.Exported = true;

                    //Consolidation is needed for Measurements records that were created 'double' for browser activity
                    //on a same domain but from different pages titles, resulting in different title hashes, resulting
                    //in distinct measurements records, but that are in fact the same subdomain and the same activity,
                    //in the case where the user chose not to track application titles.
                    
                    Measurement toConsolidate = payload.Measurements.SingleOrDefault(m => m.WindowId == windowId.Value);

                    if(toConsolidate != null)
                    {
                        logger.LogTrace($"adding - {e.ApplicationShortTitle} MC: {e.MouseCounter} KC: {e.KeyboardCounter}");
                        logger.LogTrace($"adding to - {toConsolidate.ApplicationShortTitle} MC: {toConsolidate.MouseCounter} KC: {toConsolidate.KeyboardCounter}");
                        toConsolidate.Add(e); 
                        logger.LogTrace($"added - {payload.Measurements.SingleOrDefault(m => m.WindowId == windowId.Value).ApplicationShortTitle} MC: {payload.Measurements.SingleOrDefault(m => m.WindowId == windowId.Value).MouseCounter} KC: {payload.Measurements.SingleOrDefault(m => m.WindowId == windowId.Value).KeyboardCounter}");
                    }
                    else
                    {
                        logger.LogTrace($"inserted - {e.ApplicationShortTitle} MC: {e.MouseCounter} KC: {e.KeyboardCounter} ");
                        payload.Measurements.Insert(0, e);
                    }

                    velocityService.IncreaseMetrics(e);
                }

                contextMenuViewModel.UpdateVelocityDisplays();
                logger.LogInformation($" -{payload.Measurements.Count}- ");

                for (int i = 0; i < payload.Measurements.Count; i++)
                {
                    var e = payload.Measurements.ElementAt(i);
                    logger.LogTrace($" {e.ApplicationShortTitle} MC: {e.MouseCounter} KC: {e.KeyboardCounter} ");
                }

                httpClient.PostAsJsonAsync(configuration["Endpoints:Measurements"], payload);

                foreach (var m in measurementsOfPastTimeSegments)
                {
                    measurementsService.Measurements.TryRemove(m);
                }

                logger.LogDebug("Exporting ended.");
            }
        }
    }
}