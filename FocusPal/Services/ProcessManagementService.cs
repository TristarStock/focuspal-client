﻿using FocusPal.Abstractions;
using FocusPal.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using UIAutomationClient;

namespace FocusPal
{
    public struct Rectangle
    {
        public int Left, Top, Right, Bottom;
    }
    public class ProcessManagementService : IProcessManagementService
    {
        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rectangle rectangle);

        private readonly IUserOptions userOptions;
        private readonly IContextMenuViewModel contextMenuViewModel;
        private readonly ILogger logger;

        public ProcessManagementService(IUserOptions userOptions,
                                        IContextMenuViewModel contextMenuViewModel,
                                        ILogger<ProcessManagementService> logger)
        {
            this.userOptions = userOptions;
            this.contextMenuViewModel = contextMenuViewModel;
            this.logger = logger;

            contextMenuViewModel.AppModeChanged += ContextMenuViewModel_AppModeChanged;
        }

        private void ContextMenuViewModel_AppModeChanged(Models.Enums.AppMode appMode)
        {
            List<string> processesStopped = new List<string>();

            if(appMode == Models.Enums.AppMode.Focus)
            {
                foreach (var unproductiveAppName in userOptions.FocusMode.UnproductiveApplications)
                {
                    var processes = Process.GetProcessesByName(unproductiveAppName);
                    if (processes.Length > 0)
                    {
                        processesStopped.Add(unproductiveAppName);
                        try
                        {
                            for (int i = 0; i < processes.Length; i++)
                            {
                                processes[i].Kill();
                            }
                        }
                        catch
                        {
                            //Kill() -> invalidoperationexception, notsupportedexception, win32exception
                        }
                    }
                }

                if(processesStopped.Count > 0)
                {
                    contextMenuViewModel.ShowTooltip($"Stopped {string.Join(", ", processesStopped)}.");
                }
            }
        }

        public static string GetURLFromProcess(Process process, Browser browser)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            CUIAutomation automation = new CUIAutomation();

            Rectangle windowDimensions = new Rectangle();
            GetWindowRect(process.MainWindowHandle, ref windowDimensions);

            var point = new tagPOINT() 
            { 
                x = 230 + windowDimensions.Left,
                y = 70 + windowDimensions.Top
            };

            var urlControl = automation.ElementFromPoint(point);
            if (urlControl != null && 
                urlControl.CurrentControlType == UIA_ControlTypeIds.UIA_EditControlTypeId)
            {
                var url = urlControl.GetCurrentPropertyValue(UIA_PropertyIds.UIA_ValueValuePropertyId);
                return url.ToString();
            }

            var condition1 = automation.CreatePropertyCondition(UIA_PropertyIds.UIA_ControlTypePropertyId, UIA_ControlTypeIds.UIA_EditControlTypeId);
            var condition2 = automation.CreatePropertyCondition(UIA_PropertyIds.UIA_NamePropertyId, browser.AddressPropertyName);

            urlControl = automation.ElementFromHandle(process.MainWindowHandle)
                                   .FindFirst(UIAutomationClient.TreeScope.TreeScope_Descendants,
                                         automation.CreateAndCondition(condition1, condition2));

            if (urlControl != null &&
                urlControl.CurrentControlType == UIA_ControlTypeIds.UIA_EditControlTypeId)
            {
                var url = urlControl.GetCurrentPropertyValue(UIA_PropertyIds.UIA_ValueValuePropertyId);
                return url.ToString();
            }

            return null;
        }

        public void ProcessFocusModeProcessStart(string processName, uint processId, uint parentProcessId)
        {
            processName = processName.ToLower().Replace(".exe", "");

            Debug.WriteLine($"Process started: {processName}");

            //Browser domains cannot be managed here since we didn't resolve the url yet.
            if (userOptions.FocusMode.UnproductiveApplications.Contains(processName))
            {
                try
                {
                    var process = Process.GetProcessById(Convert.ToInt32(processId));
                    contextMenuViewModel.ShowTooltip($"Prevented {processName} from starting.");
                    process.Kill();
                }
                catch (ArgumentException ex)
                {
                    //already handled/closed the process
                }
                catch
                {
                    //invalidoperationexception, notsupportedexception, win32exception
                }
            }
        }
    }
}