﻿using FocusPal.Abstractions;
using FocusPal.Models;
using FocusPal.Models.Enums;
using FocusPal.Models.Structs;
using FocusPal.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace FocusPal
{
    public interface IUserActivityService
    {
        void RegisterEventHandlers();
        void RegisterHooks();
    }

    public class UserActivityService : IUserActivityService
    {
        [DllImport("user32.dll")]   private static extern int    CallNextHookEx(int idHook, int nCode, int wParam, IntPtr lParam);
        [DllImport("user32.dll")]   private static extern int    SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId);
        [DllImport("user32.dll")]   private static extern int    UnhookWindowsHookEx(int idHook); // necessary to unfreeze the thread using breakpoints that calls UnhookWindowsHookEx, while debugging locally.
        [DllImport("user32.dll")]   private static extern int    GetWindowTextLength(IntPtr hWnd);
        [DllImport("user32.dll")]   private static extern int    GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll")]   private static extern uint   GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
        [DllImport("user32.dll")]   private static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]   private static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd); 
        [DllImport("kernel32.dll")] private static extern IntPtr OpenProcess(uint dwDesiredAccess, bool bInheritHandle, uint dwProcessId); 
        [DllImport("psapi.dll")]    private static extern uint   GetModuleFileNameEx(IntPtr hWnd, IntPtr hModule, StringBuilder lpFileName, int nSize);
        [DllImport("kernel32.dll")] private static extern bool   CloseHandle(IntPtr handle);
        

        public event EventHandler<MouseEvent>    MouseAction;
        public event EventHandler<KeyboardEvent> KeyboardAction;

        public delegate int HookProc(int nCode, int wParam, IntPtr lParam);

        private int mouseHookId = 0, keyboardHookId = 0;
        private HookProc mouseHookCallback = null;
        private GCHandle _gcHandlerMouse;
        
        private HookProc keyboardHookCallback = null;
        private GCHandle _gcHandlerKeyboard;


        private POINT prevCoords;
        private readonly ILogger<UserActivityService> logger;
        private readonly IExceptionsService<UserActivityService> exceptionsService;
        private readonly IUserOptions userOptions;
        private readonly IMeasurementHasher measurementHasher;
        private readonly IContextMenuViewModel contextMenuViewModel;
        private readonly IMeasurementsService measurementsService;
        private readonly IProcessManagementService processManagementService;

        ManagementEventWatcher processWatcher = null;

        public UserActivityService(ILogger<UserActivityService> logger, 
                                   IExceptionsService<UserActivityService> exceptionsService, 
                                   IUserOptions userOptions,
                                   IMeasurementHasher measurementHasher,
                                   IContextMenuViewModel contextMenuViewModel,
                                   IMeasurementsService measurementsService,
                                   IProcessManagementService processManagementService)
        {
            this.mouseHookCallback    = new HookProc(MouseHookCallback);
            this.keyboardHookCallback = new HookProc(KeyboardHookCallback);

            //https://stackoverflow.com/questions/69102624/executionengineexception-on-lowlevel-keyboard-hook
            this._gcHandlerMouse    = GCHandle.Alloc(this.mouseHookCallback);
            this._gcHandlerKeyboard = GCHandle.Alloc(this.keyboardHookCallback);

            this.logger = logger;
            this.exceptionsService = exceptionsService;
            this.userOptions = userOptions;
            this.measurementHasher = measurementHasher;
            this.contextMenuViewModel = contextMenuViewModel;
            this.measurementsService = measurementsService;
            this.processManagementService = processManagementService;

            contextMenuViewModel.AppModeChanged += ContextMenuViewModel_AppModeChanged;
        }

        private void ContextMenuViewModel_AppModeChanged(AppMode appMode)
        {
            if(processWatcher == null && appMode == AppMode.Focus)
            {
                string queryString = "SELECT * FROM __InstanceCreationEvent WITHIN .025 WHERE TargetInstance ISA 'Win32_Process'";
                processWatcher = new ManagementEventWatcher(@"\\.\root\CIMV2", queryString);
                processWatcher.EventArrived += ProcessStartHandler;
                processWatcher.Start();
            }
        }

        public void RegisterEventHandlers()
        {
            this.KeyboardAction += (object? sender, KeyboardEvent e) =>
            {
                try
                {
                    var hash = measurementHasher.Hash(e);

                    measurementsService.Measurements.AddOrUpdate(hash,
                        (key) =>
                        {
                            Debug.WriteLine($"K: 1");
                            return new Measurement(userOptions, 0, 0, 1, e.ApplicationName, e.ApplicationTitle, e.ApplicationTitleHash, e.Browser, e.HWnd);
                        },
                        (key, value) =>
                        {
                            value.KeyboardCounter += 1;
                            Debug.WriteLine($"K: {value.KeyboardCounter}");
                            return value;
                        });
                }
                catch (Exception ex)
                {
                    exceptionsService.ReportException(ex);
#if DEBUG
                    throw;
#endif
                }
            };

            this.MouseAction += (object? sender, MouseEvent e) =>
            {
                try
                {
                    var hash = measurementHasher.Hash(e);

                    measurementsService.Measurements.AddOrUpdate(hash,
                           (key) =>
                           {
                               Debug.WriteLine($"C: {e.ClickCount}");
                               return new Measurement(userOptions, e.ClickCount, e.Distance, 0, e.ApplicationName, e.ApplicationTitle, e.ApplicationTitleHash, e.Browser, e.HWnd);
                           },
                           (key, value) =>
                           {
                               value.MouseCounter   += e.ClickCount;
                               value.DistancePixels += e.Distance;
                               if(e.ClickCount > 0)
                                   Debug.WriteLine($"C: {value.MouseCounter}");

                               return value;
                           });
                }
                catch (Exception ex)
                {
                    exceptionsService.ReportException(ex);
#if DEBUG
                    throw;
#endif
                }
            };
        }

        public void RegisterHooks()
        {
            var hInstance = Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]);

            this.mouseHookId = SetWindowsHookEx((int)SetWindowsHookExCommands.WH_MOUSE_LL, this.mouseHookCallback, hInstance, 0);
            this.keyboardHookId = SetWindowsHookEx((int)SetWindowsHookExCommands.WH_KEYBOARD_LL, this.keyboardHookCallback, hInstance, 0);
        }

        private void ProcessStartHandler(object sender, EventArrivedEventArgs e)
        {
            if (contextMenuViewModel.Mode == AppMode.Focus)
            {
                var targetInstance = ((ManagementBaseObject)e.NewEvent.Properties["TargetInstance"].Value);

                var processName = targetInstance.Properties["Caption"].Value.ToString();
                var processId   = (uint)targetInstance.Properties["ProcessId"].Value;
                var parentProcessId = (uint)targetInstance.Properties["ParentProcessId"].Value;
                
                processManagementService.ProcessFocusModeProcessStart(processName, processId, parentProcessId);
            }
        }

        private int MouseHookCallback(int nCode, Int32 wParam, IntPtr lParam)
        {
            if (nCode >= 0 && contextMenuViewModel.Mode != AppMode.Paused)
            {
                try
                {
                    MouseLLHookStruct mouseHookStruct = (MouseLLHookStruct)Marshal.PtrToStructure(lParam, typeof(MouseLLHookStruct));

                    MouseInputNotifications button = (MouseInputNotifications)wParam;
                    
                    int clickCount = 0;
                    long distance = 0;

                    if (button == MouseInputNotifications.WM_LBUTTONUP || button == MouseInputNotifications.WM_RBUTTONUP)
                        clickCount = 1;

                    if (prevCoords != null)
                    {
                        double x = Math.Abs(prevCoords.x - mouseHookStruct.pt.x);
                        double y = Math.Abs(prevCoords.y - mouseHookStruct.pt.y);
                        distance = (long) Math.Ceiling(Math.Sqrt(x * x + y * y));
                    }

                    prevCoords = mouseHookStruct.pt;
                                        
                    var topWindowInfo = GetTopWindowInfo(userOptions);
                    
                    MouseAction?.Invoke(this, new MouseEvent()
                    {
                        ClickCount = clickCount,
                        Distance = distance,
                        ApplicationName = topWindowInfo.ApplicationName,
                        ApplicationTitle = topWindowInfo.ApplicationTitle,
                        ApplicationTitleHash = topWindowInfo.ApplicationTitleHash,
                        Browser = topWindowInfo.Browser,
                        HWnd = topWindowInfo.hWnd
                    });
                    
                }
                catch (Exception ex)
                {
                    logger.LogError("m hook");
                    return 0;
                }
            }

            return CallNextHookEx(this.mouseHookId, nCode, wParam, lParam);
        }

        private int KeyboardHookCallback(int nCode, Int32 wParam, IntPtr lParam)
        {
            try
            {
                if (nCode >= 0 && (KeyboardInputNotifications)wParam == KeyboardInputNotifications.WM_KEYDOWN && contextMenuViewModel.Mode != AppMode.Paused)
                {
                    var topWindowInfo = GetTopWindowInfo(userOptions);

                    KeyboardAction?.Invoke(this, new KeyboardEvent()
                    {
                        ApplicationName = topWindowInfo.ApplicationName,
                        ApplicationTitle = topWindowInfo.ApplicationTitle,
                        ApplicationTitleHash = topWindowInfo.ApplicationTitleHash,
                        Browser = topWindowInfo.Browser,
                        HWnd = topWindowInfo.hWnd
                    });
                }
                
                return CallNextHookEx(this.keyboardHookId, nCode, wParam, lParam);
            }
            catch (Exception ex)
            {
                logger.LogError("k hook");
                return 0;
            }
        }

        private static string GetWindowTitle(IntPtr hWnd)
        {
            int length = GetWindowTextLength(hWnd);
            
            StringBuilder sb = new StringBuilder(length + 1);
            GetWindowText(hWnd, sb, sb.Capacity);
            
            return sb.ToString();
        }

        public static TopWindowInfos GetTopWindowInfo(IUserOptions userOptions)
        {
            //windowTitle and applicationTitle are used in case user does not want to track their windows full titles
            //but we still need to get the info temporarily to md5 it and use it as a hash for chrome/domain accurate tracking

            string windowTitle, applicationTitle;

            TopWindowInfos infos = new TopWindowInfos();
            infos.hWnd = GetForegroundWindow();
            
            if (infos.hWnd != IntPtr.Zero)
            {
                windowTitle = GetWindowTitle(infos.hWnd);

                GetWindowThreadProcessId(infos.hWnd, out uint lpdwProcessId);

                IntPtr hProcess = OpenProcess(0x0410, false, lpdwProcessId);

                if (hProcess != IntPtr.Zero)
                {
                    StringBuilder text = new StringBuilder(1000);

                    GetModuleFileNameEx(hProcess, IntPtr.Zero, text, text.Capacity);
                    CloseHandle(hProcess);

                    infos.ApplicationName = Path.GetFileName(text.ToString());
                }
                else
                {
                    infos.ApplicationName = "UNKNOWN";
                }

                IntPtr hWndOwner = infos.hWnd;
                do
                {
                    applicationTitle = GetWindowTitle(hWndOwner);
                    hWndOwner = GetWindow(hWndOwner, (uint)GetWindowCommands.GW_OWNER);
                }
                while (hWndOwner != IntPtr.Zero);

                if (userOptions.ProfileConfiguration.IncludeTitles)
                {
                    infos.ApplicationTitle = applicationTitle;
                    infos.WindowTitle = windowTitle;
                }

                infos.ApplicationTitleHash = Hasher.Hash(applicationTitle);
                infos.WindowTitle = Hasher.Hash(windowTitle);
            }

            if (!String.IsNullOrEmpty(infos.ApplicationName))
            {
                infos.Browser = Browsers.GetBrowserTypeByProcessName(infos.ApplicationName);
            }

            return infos;
        }
    }
}