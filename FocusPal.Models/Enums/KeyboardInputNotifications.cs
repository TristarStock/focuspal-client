﻿namespace FocusPal.Models.Enums
{
    public enum KeyboardInputNotifications
    {
        WM_KEYDOWN = 0x100, WM_KEYUP = 0x101, WM_SYSKEYDOWN = 0x104, WM_SYSKEYUP = 0x105, VK_SHIFT = 0x10, VK_CAPITAL = 0x14, VK_NUMLOCK = 0x90
    }
}