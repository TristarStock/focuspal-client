﻿namespace FocusPal.Models.Enums
{
    public enum MouseButtons
    {
        None = 0x0,
        Left = 0x100000,
        Right = 0x200000,
        Middle = 0x400000,
        XButton1 = 0x800000,
        XButton2 = 0x1000000
    }
}