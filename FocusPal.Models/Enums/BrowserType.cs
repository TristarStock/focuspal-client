﻿namespace FocusPal.Models.Enums
{
    public enum BrowserType { Chrome, Firefox, IE, Edge, Opera, Safari }
}