﻿namespace FocusPal.Models.Enums
{
    public enum MonitorDpiType
    {
        Effective = 0,
        Angular = 1,
        Raw = 2,
    }
}