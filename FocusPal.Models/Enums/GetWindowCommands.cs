﻿namespace FocusPal.Models.Enums
{
    public enum GetWindowCommands : uint
    {
        GW_HWNDFIRST, GW_HWNDLAST, GW_HWNDNEXT, GW_HWNDPREV, GW_OWNER, GW_CHILD
    }
}