﻿namespace FocusPal.Models.Enums
{
    public enum AppMode { Active, Paused, Focus }
}