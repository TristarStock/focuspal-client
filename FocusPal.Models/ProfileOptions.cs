﻿namespace FocusPal.Models
{
    public class ProfileOptions
    {
        public bool IncludeTitles { get; set; }
        public int FrequencyInMinutes { get; set; }
    }
    public class FocusModeOptions
    {
        public List<string> UnproductiveApplications { get; set; }
        public List<string> UnproductiveDomains { get; set; }
        public List<string> UnproductiveSubdomains { get; set; }
    }
}