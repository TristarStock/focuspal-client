﻿namespace FocusPal.Models
{
    public static class DateTimeExtensions
    {
        public static DateTime RoundDown(this DateTime date, TimeSpan timeSpan)
        {
            var delta = date.Ticks % timeSpan.Ticks;
            return new DateTime(date.Ticks - delta, date.Kind);
        }
    }
}