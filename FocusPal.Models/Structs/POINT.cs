﻿using System.Runtime.InteropServices;

namespace FocusPal.Models.Structs
{
    /// <summary>
    /// The POINT structure defines the x- and y- coordinates of a point. 
    /// </summary>
    /// <remarks>
    /// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/gdi/rectangl_0tiq.asp
    /// </remarks>
    [StructLayout(LayoutKind.Sequential)]
    public class POINT
    {
        /// <summary>
        /// Specifies the x-coordinate of the point. 
        /// </summary>
        public int x;
        /// <summary>
        /// Specifies the y-coordinate of the point. 
        /// </summary>
        public int y;
    }
}