﻿using FocusPal.Abstractions;
using FocusPal.Models.Enums;
using Microsoft.Extensions.Options;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace FocusPal.Models
{
    public class AppOptions
    {
        public bool DisplayProductivityHourly { get; set; }
    }
    public interface IContextMenuViewModel
    {
        event ContextMenuViewModel.TooltipEventHandler TooltipNeeded;
        event ContextMenuViewModel.AppModeChangeEventHandler AppModeChanged;

        AppMode Mode { get; set; }
        void UpdateVelocityDisplays();
        void ShowTooltip(string message);
        void SetDisplayProductivityHourly(bool b);
    }
    public class ContextMenuViewModel : IContextMenuViewModel, INotifyPropertyChanged
    {
        public delegate void TooltipEventHandler(string message);
        public event TooltipEventHandler TooltipNeeded;

        public delegate void AppModeChangeEventHandler(AppMode appMode);
        public event AppModeChangeEventHandler AppModeChanged;


        private AppMode mode, previousMode;
        private readonly IVelocityService velocityService;
        private readonly IOptionsMonitor<AppOptions> appOptions;

        public ContextMenuViewModel(IVelocityService velocityService, 
                                    IOptionsMonitor<AppOptions> appOptions)
        {
            this.velocityService = velocityService;
            this.appOptions = appOptions;

            this.velocityService.MetricsChanged += VelocityService_MetricsChanged;
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        
        public AppMode Mode {
            get => mode; 
            set {
                if (mode == AppMode.Paused && value == AppMode.Paused) //resume
                    mode = previousMode;
                else
                {
                    previousMode = mode;
                    mode = value;
                }

                OnPropertyChanged("FocusHeader");
                OnPropertyChanged("PausedHeader");
                OnPropertyChanged("StatusHeader");
                OnPropertyChanged("StatusIconPath");

                if (mode == AppMode.Focus)
                {
                    //velocityService.ResetMetrics();
                    AppModeChanged?.Invoke(mode);
                }
            }
        }

        public bool DisplayProductivityHourly => appOptions.CurrentValue.DisplayProductivityHourly;

        public string FocusHeader  { get => $"Focus Mode: {(mode == AppMode.Focus ? "On" : "Off")}"; }
        public string PausedHeader { get => mode == AppMode.Paused ? "Resume" : "Pause"; }
        public string StatusHeader { get => $"FocusPal is {(mode == AppMode.Paused ? "paused" : "running")}"; }

        public string KlmsHeader   => DisplayProductivityHourly ? 
                                      (velocityService.Minutes == 0 ? "—" : ((velocityService.TotalInches / 39370.0) / (velocityService.Minutes / 60.0)).ToString("N3", CultureInfo.InvariantCulture))
                                      : (velocityService.TotalInches / 39370.0).ToString("N3", CultureInfo.InvariantCulture);
        public string KeysHeader   => DisplayProductivityHourly ? 
                                      (velocityService.Minutes == 0 ? "—" : (velocityService.TotalKeysCount / (velocityService.Minutes / 60.0)).ToString("N0", CultureInfo.InvariantCulture))
                                      : velocityService.TotalKeysCount.ToString("N0", CultureInfo.InvariantCulture);
        public string ClicksHeader => DisplayProductivityHourly ? 
                                      (velocityService.Minutes == 0 ? "—" : (velocityService.TotalClicksCount / (velocityService.Minutes / 60.0)).ToString("N0", CultureInfo.InvariantCulture))
                                      : velocityService.TotalClicksCount.ToString("N0", CultureInfo.InvariantCulture);

        public string DisplayProductivityHourlyImage => DisplayProductivityHourly ? "Images/Buttons/checked.png" : "";
        public string DisplayProductivityTotalImage  => DisplayProductivityHourly ? "" : "Images/Buttons/checked.png";

        public string ClicksLabel => DisplayProductivityHourly ? "Clicks/h:" : "Clicks:";
        public string KlmsLabel   => DisplayProductivityHourly ? "Klms/h:" : "Klms:";
        public string KeysLabel   => DisplayProductivityHourly ? "Keys/h:" : "Keys:";

        public string StatusIconPath
        {
            get
            {
                if (mode == AppMode.Focus)
                    return "/Images/Statuses/focus.ico";

                if (mode == AppMode.Paused)
                    return "/Images/Statuses/paused.ico";

                return "/Images/Statuses/active.ico";
            }
        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void VelocityService_MetricsChanged(object? sender, EventArgs e) => UpdateVelocityDisplays();

        public void UpdateVelocityDisplays()
        {
            OnPropertyChanged("ClicksHeader");
            OnPropertyChanged("KeysHeader");
            OnPropertyChanged("KlmsHeader");
        }

        public void ShowTooltip(string title) => TooltipNeeded?.Invoke(title);

        public void SetDisplayProductivityHourly(bool b)
        {
            if (b == appOptions.CurrentValue.DisplayProductivityHourly)
                return;

            appOptions.CurrentValue.DisplayProductivityHourly = b;

            UpdateAppSettings(b);

            OnPropertyChanged("DisplayProductivityHourlyImage");
            OnPropertyChanged("DisplayProductivityTotalImage");
            OnPropertyChanged("ClicksLabel");
            OnPropertyChanged("KeysLabel");
            OnPropertyChanged("KlmsLabel");
            OnPropertyChanged("KlmsHeader");
            OnPropertyChanged("ClicksHeader");
            OnPropertyChanged("KeysHeader");
        }
        public void UpdateAppSettings(bool displayProductivityHourly)
        {
            var configJson = File.ReadAllText("appsettings.json");
            var config     = JsonSerializer.Deserialize<Dictionary<string, object>>(configJson);
            var appOptions = JsonSerializer.Deserialize<AppOptions>(config["AppOptions"].ToString());

            appOptions.DisplayProductivityHourly = displayProductivityHourly;

            config["AppOptions"] = appOptions;

            var updatedConfigJson = JsonSerializer.Serialize(config, new JsonSerializerOptions { WriteIndented = true });
            File.WriteAllText("appsettings.json", updatedConfigJson);
        }
    }
}