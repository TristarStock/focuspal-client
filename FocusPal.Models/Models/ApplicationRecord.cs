﻿namespace FocusPal.Models
{
    public class ApplicationRecord
    {
        public int Id { get; set; }
        public string Hash { get; set; } = String.Empty;
        public string ApplicationName { get; set; } = String.Empty;
        public string ApplicationTitle { get; set; } = String.Empty;
        public string ApplicationShortTitle { get; set; } = String.Empty;
    }
}