﻿namespace FocusPal.Models
{
    public interface IUserOptions
    {
        ProfileOptions ProfileConfiguration { get; set; }
        FocusModeOptions FocusMode { get; set; }
    }

    public class UserOptions : IUserOptions
    {
        public ProfileOptions ProfileConfiguration { get; set; }
        public FocusModeOptions FocusMode { get; set; }
    }
}
