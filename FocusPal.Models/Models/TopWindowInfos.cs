﻿namespace FocusPal.Models
{
    public class TopWindowInfos
    {
        public string WindowTitle { get; set; }
        public string ApplicationTitle { get; set; } = String.Empty;
        /// <summary>
        /// This property was added so that we can have separate record of using chrome per domain 
        /// without keep the ApplicationTitle in memory for users who wish to not track that information.
        /// </summary>
        public string ApplicationTitleHash { get; set; } = String.Empty;
        public string ApplicationName { get; set; }
        public Browser Browser { get; set; }
        public IntPtr hWnd { get; set; }
    }
}