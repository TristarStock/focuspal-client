﻿using FocusPal.Abstractions;

namespace FocusPal.Models
{
    public class Measurement : ICloneable, IMeasurement
    {
        public Measurement(
                        IUserOptions options,
                        int mouseCounter = 0,
                        long distance = 0,
                        int keyboardCounter = 0,
                        string applicationName = "",
                        string applicationTitle = "",
                        string applicationTitleHash = "",
                        Browser browser = null,
                        IntPtr hWnd = default)
        {
            MouseCounter = mouseCounter;
            DistancePixels = distance;
            KeyboardCounter = keyboardCounter;
            ApplicationName = applicationName;
            ApplicationTitle = applicationTitle;
            ApplicationTitleHash = applicationTitleHash;
            Browser = browser;
            HWnd = hWnd;

            TimeSegment = DateTime.Now.RoundDown(TimeSpan.FromMinutes(options.ProfileConfiguration.FrequencyInMinutes)).ToString("yyyy-MM-dd HH:mm");
        }

        public DateTime TimestampDate { get; set; } = DateTime.Now;
        public string Timestamp { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        public string TimestampShort { get; set; } = DateTime.Now.ToString("HH:mm");
        public int MouseCounter { get; set; } = 0;
        public long DistancePixels { get; set; } = 0;
        public long DistanceInches { get; set; } = 0;
        public int KeyboardCounter { get; set; } = 0;
        public string ApplicationName { get; set; } = "";
        public string ApplicationTitle { get; set; } = "";
        public string ApplicationTitleHash { get; set; } = "";
        public string ApplicationShortTitle { get { return this.Domain; } }
        public Browser Browser { get; }
        public string Domain { get; set; } = "";

        public int DomainRetries = 0;
        public IntPtr HWnd { get; }
        public int WindowId { get; set; } = 0;
        public bool Exported { get; set; } = false;
        public string TimeSegment { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public void Add(Measurement m)
        {
            this.KeyboardCounter += m.KeyboardCounter;
            this.DistanceInches  += m.DistanceInches;
            this.DistancePixels  += m.DistancePixels;
            this.MouseCounter    += m.MouseCounter;

            m.Exported = true;
        }
    }
}