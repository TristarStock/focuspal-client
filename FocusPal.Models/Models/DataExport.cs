﻿using System.Text.Json.Serialization;

namespace FocusPal.Models
{
    public class DataExport
    {
        public List<Measurement> Measurements { get; set; } = new List<Measurement>();
        public List<ApplicationRecord> Windows { get; set; } = new List<ApplicationRecord>();
    }
}