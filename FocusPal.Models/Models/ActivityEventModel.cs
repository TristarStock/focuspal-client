﻿namespace FocusPal.Models
{
    public abstract class ActivityEventModel
    {
        public Browser Browser { get; set; }
        public string? ApplicationName { get; set; }
        public string? ApplicationTitle { get; set; }
        public string ApplicationTitleHash { get; set; }
        public IntPtr HWnd { get; set; }
    }
    public class MouseEvent : ActivityEventModel
    {
        public int ClickCount { get; set; }
        public long Distance { get; set; }
    }

    public class KeyboardEvent : ActivityEventModel
    {
    }
}