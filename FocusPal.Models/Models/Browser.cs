﻿using FocusPal.Models.Enums;

namespace FocusPal.Models
{
    public class Browser
    {
        public BrowserType BrowserType { get; set; }
        public string AddressPropertyName { get; set; }
        public string ProcessName { get; set; }
    }
}