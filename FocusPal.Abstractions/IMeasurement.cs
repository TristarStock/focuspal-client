﻿namespace FocusPal.Abstractions
{
    public interface IMeasurement
    {
        string TimeSegment { get; set; }
        long DistanceInches { get; set; }
        int KeyboardCounter { get; set; }
        int MouseCounter { get; set; }
    }
}