﻿namespace FocusPal.Abstractions
{
    public interface IVelocityService
    {
        long TotalInches { get; }
        int TotalClicksCount { get; }
        int TotalKeysCount { get; }
        double Minutes { get; }

        event EventHandler MetricsChanged;

        void IncreaseMetrics(IMeasurement m);
        void ResetMetrics();
    }
}