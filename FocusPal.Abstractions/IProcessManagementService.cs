﻿namespace FocusPal.Abstractions
{
    public interface IProcessManagementService
    {
        void ProcessFocusModeProcessStart(string processName, uint processId, uint parentProcessId);
    }
}